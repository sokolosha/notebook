from django.shortcuts import render, redirect
from .forms import UserForm
from .models import Person
from django.http import HttpResponseNotFound
from django.contrib.auth import authenticate, login


def index(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/notebook/')
        else:
            return redirect('/')
    else:
        userform = UserForm()
        return render(request, "index.html", {"form": userform})


def list(request):
    people = Person.objects.all()
    return render(request, "notebook.html", {"people": people})


def create(request):
    if request.method == "POST":
        tom = Person()
        tom.name = request.POST.get("name")
        tom.number = request.POST.get("number")
        tom.save()
        return redirect('/notebook/')


def edit(request, id):
    try:
        person = Person.objects.get(id=id)

        if request.method == "POST":
            person.name = request.POST.get("name")
            person.number = request.POST.get("number")
            person.save()
            return redirect('/notebook/')
        else:
            return render(request, "edit.html", {"person": person})
    except Person.DoesNotExist:
        return HttpResponseNotFound("<h2>Person not found</h2>")


def delete(request, id):
    try:
        person = Person.objects.get(id=id)
        person.delete()
        return redirect('/notebook/')
    except:
        return HttpResponseNotFound("<h2>Person not found</h2>")
