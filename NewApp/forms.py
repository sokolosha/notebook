from django import forms

class UserForm(forms.Form):
    username = forms.CharField(label="Login")
    password = forms.CharField(label="Password")

